﻿// ClassHomework.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

class Vector 
{
public: 
    Vector() : x(0), y(0), z(0)
    {}

    private:
    double x;
    double y;
    double z; 

public:
    Vector(double x_, double y_, double z_): x(x_),y(y_),z(z_)
    {}

    // создаем функцию для проверки корректоности работы вектора
    void Show() {
        cout << x << ' ' << y << ' ' << z;
}

    // вычисляем длину вектора
    void VectorLength()
    {
        double vLength = sqrt(x * x + y * y + z * z);
        cout << "\n" << vLength;
    }
};
int main()
{
   // проверяем, что вектор печатается корректно
    Vector v(7,7,7);
    v.Show();

   // выводим модуль вектора на консоль 
    v.VectorLength();
}
